import HyperService from './hyper-service';
const https = new HyperService();
const URL = 'https://jsonplaceholder.typicode.com/users';
const headers = {"Accept": "application/json"};

export const getUser=()=>{
    return https.get(URL,headers);
}
