import axios from 'axios';

class HyperService{

    get = (url, headers)=>{
        return axios.request({
            url: url,
            method: "get",
            headers: headers
        });
    }

    post = (url, data, headers) => {
        return axios.request({
            url: url,
            method: "post",
            data: data,
            headers: headers
        });
    };
  
    delete = (url, headers)=>{
        return axios.request({
            url: url,
            method: "delete",
            headers: headers
        });
    }

}
export default HyperService;