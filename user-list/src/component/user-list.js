import React,{Component} from 'react';
import './user-list-style.css';
import {getUser} from '../services/REST-APIs';

class UserList extends Component {
    constructor(props){
        super(props);
        this.state={
           userList:[], 
           editUserList:[],
        }
    }

   // loading user list on page loads  
   componentDidMount(){
      this.getUserList();
    }

   // get user list from 'https://jsonplaceholder.typicode.com/users
   getUserList=()=>{
      getUser().then((response) => {
        this.setState({userList:response.data})
               })
        .catch((error) => {
             console.log('Error in geting user list', error);
           });
    }
    
    // edit function for edit the selected user
    clickEdit =(id)=>{
        var edituser =[];
        var newUserList =[...this.state.userList];
        for(var x in newUserList){
         if(newUserList[x].id===id){
            edituser.push(newUserList[x]);
         }
      }
        this.setState({editUserList:edituser});
        }

    // function for delete the user
    clickDelete =(id)=>{
       var newUserList =[...this.state.userList];
       var index = newUserList.findIndex(obj => obj.id === id);
       newUserList.splice(index,1);
       this.setState({userList:newUserList});
    }
    //edit user details
    editUserDate=(e,id,params)=>{
     var editList = [...this.state.userList];
     var index = editList.findIndex(obj => obj.id === id);
     if(params==="name"){
      editList[index].name = e.target.value;
     }
     if(params==="email"){
      editList[index].email = e.target.value;
     }
     if(params==="phone"){
      editList[index].phone = e.target.value;
     }
     this.setState({editList});
    }

   //submit function for after edit the user
    submit=()=>{
       var newarray = [...this.state.userList]
       this.setState({userList:newarray})
    }
  render(){
  return(
   <div> 
     <h1 id='title'>User Table</h1>
    <table id='employee'>
      <thead>
         <tr>
            <th >ID</th>
            <th >Name</th>
            <th >Email</th>
            <th >Phone</th>
            <th >Action</th>
         </tr>
      </thead>
      <tbody>
         {this.state.userList.map((user,inx) => { 
         return(
         <tr key={inx}>
            <td>{user.id}</td>
            <td>{user.name}</td>
            <td>{user.email}</td>
            <td>{user.phone}</td>
            <td className='opration'>
               <button type="button" className="btn btn-primary button-margin" data-toggle="modal" data-target="#staticBackdrop" onClick={()=>this.clickEdit(user.id)}><i class="far fa-edit"></i>&nbsp; Edit</button>
               <button type="button" className="btn btn-danger button-margin" onClick={()=>this.clickDelete(user.id)}><i class="far fa-trash-alt"></i>&nbsp; Delete</button>
            </td>
         </tr>
         )})}
      </tbody>
   </table>
   {/* <!-- Modal --> */}
   <div className="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div className="modal-dialog modal-dialog-centered modal-xl">
         <div className="modal-content">
            <div className="modal-header">
               <h5 className="modal-title" id="staticBackdropLabel">Edit User</h5>
               <button type="button" className="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div className="modal-body">
               <table id='employee' className="model-user">
                  <thead>
                     <tr>
                        <th >Name</th>
                        <th >Email</th>
                        <th >Phone</th>
                     </tr>
                  </thead>
                  <tbody>
                     { this.state.editUserList.map((user,inx)=>{
                        return(
                           <tr key={inx}>
                           <td> <input type="text" className="model-input" value={user.name} onChange={(e)=>this.editUserDate(e,user.id,'name')}/></td>
                           <td> <input type="text" className="model-input" value={user.email} onChange={(e)=>this.editUserDate(e,user.id,'email')}/></td>
                           <td> <input type="text" className="model-input" value={user.phone} onChange={(e)=>this.editUserDate(e,user.id,'phone')}/></td>
                        </tr>
                        )
                     })}

                  </tbody>
               </table>
            </div>
            <div className="modal-footer">
               <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
               <button type="submit" className="btn btn-primary" data-dismiss="modal" onClick={this.submit}>Submit</button>
            </div>
         </div>
      </div>
   </div>
</div>
        )
    }
}
export default UserList;