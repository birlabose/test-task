// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
import UserList from './component/user-list'
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={UserList}  />
      </Switch>
    </Router>
  );
}

export default App;
